/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <stdexcept>
#include <string>
#include <complex>
#include <vector>
#include <thread>
#include <optional>

#include <boost/program_options.hpp>

#include "IQLogReader.h"
#include "rts/DurationTracker.h"
#include "rts/SomfyDecoder.h"
#include "rts/backend/rtlsdr/OOKDecoder.h"

#include "FrameListener.h"

namespace
{
	constexpr double DEFAULT_TOLERANCE = 0.1;
	constexpr uint32_t DEFAULT_SAMPLE_RATE = 2600000; // 2.6 MHz

	void decodeIqLog(const std::string & iqLogFileName, double tolerance, uint32_t sampleRate)
	{
		IQLogReader iqLogReader(iqLogFileName);
		rts::OOKDecoder<IQLogReader> ookDecoder(iqLogReader, sampleRate);
		rts::DurationTracker<rts::OOKDecoder<IQLogReader>> durationTracker(ookDecoder);
		FrameListener frameListener;
		rts::SomfyDecoder decoder(durationTracker, tolerance, frameListener);

		decoder.run();
	}
}

int main(int argc, char * argv[])
{
	try
	{
		std::string inputFileName;
		uint32_t sampleRate = DEFAULT_SAMPLE_RATE;
		double tolerance = DEFAULT_TOLERANCE;

		boost::program_options::options_description argDescription("Available options");
		argDescription.add_options()
			("file,f", boost::program_options::value(&inputFileName)->required(),
				"Source file name. (IQ format sampled at 2.6 MHz.)"
			)
			("sample-rate,s", boost::program_options::value(&sampleRate),
				(std::string("Sample rate of the intput file. Default: ") + std::to_string(sampleRate)).c_str())
			("tolerance,t", boost::program_options::value(&tolerance),
				(std::string("Tolerance in measured timing. Default: ") + std::to_string(tolerance)).c_str())
			("help,h", "print this help")
		;

		boost::program_options::variables_map variablesMap;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, argDescription), variablesMap);

		if (variablesMap.count("help"))
		{
			std::cout << argDescription << '\n';
			return 1;
		}

		boost::program_options::notify(variablesMap);

		decodeIqLog(inputFileName, tolerance, sampleRate);
	}
	catch (const boost::program_options::error & e)
	{
		std::cerr << e.what() << '\n';
		return 1;
	}

	return 0;
}
