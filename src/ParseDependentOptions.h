/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PARSE_DEPENDENT_OPTIONS_H
#define PARSE_DEPENDENT_OPTIONS_H

#include <utility>
#include <vector>
#include <string>
#include <map>
#include <iostream>
#include <sstream>

#include <boost/program_options.hpp>

// helper for looking up options with default values
template<typename T>
T valueOr(const boost::program_options::variables_map & vm, const std::string & key, T defaultValue)
{
	auto it = vm.find(key);
	if (it != vm.end())
		return it->second.as<T>();
	else
		return defaultValue;
}

template<typename TReceiver>
using BackendHandler = void(*)(TReceiver & receiver, const boost::program_options::variables_map & variablesMap);

template<typename TReceiver>
using BackendDescriptions = std::map<std::string, std::pair<boost::program_options::options_description, BackendHandler<TReceiver>>>;

template<typename TReceiver>
std::string getBackendOptionDescription(const BackendDescriptions<TReceiver> & backendDescriptions)
{
	std::stringstream s;
	s << "Selects librts backend. One of:";
	bool first = true;
	for (auto & backendDescription : backendDescriptions)
	{
		if (first)
			first = false;
		else
			s << ',';

		s << ' ' << backendDescription.first;
	}

	return s.str();
}

template<typename TReceiver>
std::optional<boost::program_options::variables_map> parseDependentOptions(int argc, char * argv[],
		const boost::program_options::options_description & toolOptions,
		const BackendDescriptions<TReceiver> & backendDescriptions,
		TReceiver & paramsReceiver)
{
	namespace po = boost::program_options;

	static const std::string OPTION_HELP = "help";
	static const std::string OPTION_BACKEND = "backend";

	po::options_description commonOptions;

	// don't use commonOptions.add because this would add a separate group

	// make sure backend is the 1st option
	commonOptions.add_options()
		((OPTION_BACKEND + ",B").c_str(), po::value<std::string>()->required(),
				getBackendOptionDescription(backendDescriptions).c_str())
		;

	for (const auto & option : toolOptions.options())
		commonOptions.add(option);

	// ... and help the last
	commonOptions.add_options()
		((OPTION_HELP + ",h").c_str(), "print this help")
		;

	po::parsed_options parsedOptions = po::command_line_parser(argc, argv)
		.options(commonOptions)
		.allow_unregistered()
		.run();

	std::vector<std::string> unrecognizedOptions = po::collect_unrecognized(parsedOptions.options, po::include_positional);

	po::variables_map variablesMap;
	po::store(parsedOptions, variablesMap);

	if (variablesMap.count(OPTION_HELP) != 0)
	{
		std::cout << commonOptions << '\n';
		for (const auto & backendDescription : backendDescriptions)
			std::cout << backendDescription.second.first << '\n';

		return std::nullopt;
	}

	if (variablesMap.count(OPTION_BACKEND) != 0)
	{
		auto it = backendDescriptions.find(variablesMap[OPTION_BACKEND].as<std::string>());
		if (it != backendDescriptions.end())
		{
			po::variables_map backendVariablesMap;
			po::store(po::command_line_parser(unrecognizedOptions).options(it->second.first).run(), backendVariablesMap);
			po::notify(backendVariablesMap);
			(it->second.second)(paramsReceiver, backendVariablesMap);
		}
		else
			throw std::runtime_error("unknown backend type");
	}
	// else: let po::notify handle the missing required "backend" option

	po::notify(variablesMap);

	return variablesMap;
}

#endif // PARSE_DEPENDENT_OPTIONS_H
