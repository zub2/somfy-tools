/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ParseTransmitOptions.h"
#include "ParseOptionsCommon.h"

#include "ParseDependentOptions.h"

namespace po = boost::program_options;

namespace
{
#ifdef RTS_HAVE_GPIOD
	const std::string OPEN_DRAIN = "open-drain";
#endif
	const std::string DEBUG_LOG_FILE = "log-file";
	const std::string HOST = "host";

	namespace frame_transmitter
	{
		po::options_description getRPiGPIOBackendTransmitOptions()
		{
			po::options_description rpiGPIOBackendOptions(RPI_GPIO_DESCRIPTION_NAME);
			rpiGPIOBackendOptions.add_options()
				((GPIO_NR + ",n").c_str(), po::value<unsigned>()->required(),
					"The GPIO number to use.")
			;
			return rpiGPIOBackendOptions;
		}

		void storeRPiGPIOBackendOptions(TransmitBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onRPiGPIOBackendParams(variablesMap[GPIO_NR].as<unsigned>());
		}

#ifdef RTS_HAVE_GPIOD
		po::options_description getGPIODBackendTransmitOptions()
		{
			po::options_description gpiodBackendOptions(GPIOD_DESCRIPTION_NAME);
			gpiodBackendOptions.add_options()
				((CHIP + ",C").c_str(), po::value<std::filesystem::path>()->required(),
					"Path of the GPIO chip device, e.g. /dev/gpiochip0.")
				((LINE + ",L").c_str(), po::value<unsigned>()->required(),
					"GPIO line number.")
				(ACTIVE_LOW.c_str(),
					"Invert logic levels.")
				(OPEN_DRAIN.c_str(),
					"Turn output into open-drain mode if supported.")
			;

			return gpiodBackendOptions;
		}

		void storeGPIODBackendOptions(TransmitBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onGPIODBackendParams(
				variablesMap[CHIP].as<std::filesystem::path>(),
				variablesMap[LINE].as<unsigned>(),
				variablesMap.count(OPEN_DRAIN) != 0,
				variablesMap.count(ACTIVE_LOW) != 0
			);
		}
#endif

		po::options_description getESP8266RTSBackendTransmitOptions()
		{
			po::options_description esp8266RTSBackendOptions(ESP8266_RTS_DESCRIPTION_NAME);
			esp8266RTSBackendOptions.add_options()
				((HOST + ",H").c_str(), po::value<std::string>()->required(),
					"The ESP8266-RTS host (either a hostname or IP address).")
			;

			return esp8266RTSBackendOptions;
		}

		po::options_description getDebugBackendTransmitOptions()
		{
			po::options_description debugBackendOptions(DEBUG_DESCRIPTION_NAME);
			debugBackendOptions.add_options()
				((DEBUG_LOG_FILE + ",f").c_str(), po::value<std::string>()->required(),
					"Write pulses that would be transmitted to a file.")
			;

			return debugBackendOptions;
		}

		void storeESP8266RTSBackendOptions(TransmitBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onESP8266RTSParams(
				variablesMap[HOST].as<std::string>()
			);
		}

		void storeDebugBackendOptions(TransmitBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onDebugBackendParams(
				variablesMap[DEBUG_LOG_FILE].as<std::string>()
			);
		}

		BackendDescriptions<TransmitBackendParametersReceiver> makeBackendDescriptions()
		{
			BackendDescriptions<TransmitBackendParametersReceiver> backendOptions;

			backendOptions.emplace(BACKEND_RPI_GPIO, std::pair(getRPiGPIOBackendTransmitOptions(), &storeRPiGPIOBackendOptions));
#ifdef RTS_HAVE_GPIOD
			backendOptions.emplace(BACKEND_GPIOD, std::pair(getGPIODBackendTransmitOptions(), &storeGPIODBackendOptions));
#endif // RTS_HAVE_GPIOD
			backendOptions.emplace(BACKEND_ESP8266_RTS, std::pair(getESP8266RTSBackendTransmitOptions(), &storeESP8266RTSBackendOptions));
			backendOptions.emplace(BACKEND_DEBUG, std::pair(getDebugBackendTransmitOptions(), &storeDebugBackendOptions));

			return backendOptions;
		}
	}

	namespace gpio_transmitter
	{
		po::options_description getRPiGPIOBackendTransmitOptions()
		{
			// the options are the same as for the GPIOD backend
			return frame_transmitter::getRPiGPIOBackendTransmitOptions();
		}

		void storeRPiGPIOBackendOptions(GPIOTransmitBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onRPiGPIOBackendParams(variablesMap[GPIO_NR].as<unsigned>());
		}

#ifdef RTS_HAVE_GPIOD
		po::options_description getGPIODBackendTransmitOptions()
		{
			// the options are the same as for the RPi GPIO backend
			return frame_transmitter::getGPIODBackendTransmitOptions();
		}

		void storeGPIODBackendOptions(GPIOTransmitBackendParametersReceiver & receiver, const po::variables_map & variablesMap)
		{
			receiver.onGPIODBackendParams(
				variablesMap[CHIP].as<std::filesystem::path>(),
				variablesMap[LINE].as<unsigned>(),
				variablesMap.count(OPEN_DRAIN) != 0,
				variablesMap.count(ACTIVE_LOW) != 0
			);
		}
#endif

		BackendDescriptions<GPIOTransmitBackendParametersReceiver> makeGPIOBackendDescriptions()
		{
			BackendDescriptions<GPIOTransmitBackendParametersReceiver> backendOptions;

			backendOptions.emplace(BACKEND_RPI_GPIO, std::pair(getRPiGPIOBackendTransmitOptions(), &storeRPiGPIOBackendOptions));
#ifdef RTS_HAVE_GPIOD
			backendOptions.emplace(BACKEND_GPIOD, std::pair(getGPIODBackendTransmitOptions(), &storeGPIODBackendOptions));
#endif // RTS_HAVE_GPIOD

			return backendOptions;
		}
	}
}

std::optional<po::variables_map> parseTransmitOptions(int argc, char * argv[],
		const po::options_description & toolOptions,
		TransmitBackendParametersReceiver & paramsReceiver)
{
	return parseDependentOptions(argc, argv, toolOptions, frame_transmitter::makeBackendDescriptions(), paramsReceiver);
}

std::optional<po::variables_map> parseGPIOTransmitOptions(int argc, char * argv[],
		const po::options_description & toolOptions,
		GPIOTransmitBackendParametersReceiver & paramsReceiver)
{
	return parseDependentOptions(argc, argv, toolOptions, gpio_transmitter::makeGPIOBackendDescriptions(), paramsReceiver);
}
