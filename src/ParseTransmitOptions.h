/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PARSE_TRANSMIT_OPTIONS_H
#define PARSE_TRANSMIT_OPTIONS_H

#include <optional>
#include <string>
#include <filesystem>

#include <boost/program_options.hpp>

class TransmitBackendParametersReceiver
{
public:
	virtual void onRPiGPIOBackendParams(unsigned gpioNr) = 0;
#ifdef RTS_HAVE_GPIOD
	virtual void onGPIODBackendParams(const std::filesystem::path & chipPath, unsigned line, bool openDrain, bool activeLow) = 0;
#endif
	virtual void onESP8266RTSParams(const std::string & host) = 0;
	virtual void onDebugBackendParams(const std::string & logFile) = 0;

protected:
	virtual ~TransmitBackendParametersReceiver() = default;
};

std::optional<boost::program_options::variables_map> parseTransmitOptions(int argc, char * argv[],
		const boost::program_options::options_description & toolOptions, TransmitBackendParametersReceiver & paramsReceiver);

class GPIOTransmitBackendParametersReceiver
{
public:
	virtual void onRPiGPIOBackendParams(unsigned gpioNr) = 0;
#ifdef RTS_HAVE_GPIOD
	virtual void onGPIODBackendParams(const std::filesystem::path & chipPath, unsigned line, bool openDrain, bool activeLow) = 0;
#endif

protected:
	virtual ~GPIOTransmitBackendParametersReceiver() = default;
};

std::optional<boost::program_options::variables_map> parseGPIOTransmitOptions(int argc, char * argv[],
		const boost::program_options::options_description & toolOptions, GPIOTransmitBackendParametersReceiver & paramsReceiver);

#endif // PARSE_TRANSMIT_OPTIONS_H
