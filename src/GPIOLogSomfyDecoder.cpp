/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rts/Clock.h"
#include "rts/DurationTracker.h"
#include "rts/SomfyDecoder.h"

#include "SigIntHandler.h"
#include "FrameListener.h"
#include "GPIOLogReader.h"

#include <iostream>
#include <chrono>

#include <boost/program_options.hpp>

namespace
{
	constexpr double DEFAULT_TOLERANCE = 0.1;

	void decodeFromFile(const std::string & fileName, double tolerance)
	{
		GPIOLogReader reader(fileName);
		FrameListener frameListener;
		rts::SomfyDecoder decoder(reader, tolerance, frameListener);

		decoder.run();
	}
}

int main(int argc, char * argv[])
{
	try
	{
		std::string inputFile;
		double tolerance = DEFAULT_TOLERANCE;

		boost::program_options::options_description argDescription("Available options");
		argDescription.add_options()
			("input-file,f", boost::program_options::value(&inputFile)->required(),
				"GPIO log file to read instead of real GPIO.")
			("tolerance,t", boost::program_options::value(&tolerance),
				(std::string("Tolerance in measured timing. Default: ") + std::to_string(tolerance)).c_str())
			("help,h", "print this help")
		;

		boost::program_options::variables_map variablesMap;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, argDescription), variablesMap);

		if (variablesMap.count("help"))
		{
			std::cout << argDescription << '\n';
			return 1;
		}

		boost::program_options::notify(variablesMap);

		decodeFromFile(inputFile, tolerance);
	}
	catch (const boost::program_options::error & e)
	{
		std::cerr << e.what() << '\n';
		return 1;
	}

	return 0;
}
