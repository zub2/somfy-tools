/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <memory>
#include <iostream>
#include <vector>
#include <type_traits>
#include <limits>
#include <optional>

#include "rts/SomfyFrame.h"
#include "rts/IFrameTransmitter.h"
#include "rts/FrameTransmitterFactory.h"

#include <boost/program_options.hpp>
#include <boost/container/flat_map.hpp>
#include <boost/any.hpp>

#include "GPIOLogWriter.h"
#include "ParseTransmitOptions.h"

using namespace std::literals;

namespace po = boost::program_options;

namespace
{
	constexpr size_t DEFAULT_REPEAT_FRAMES = 1;

	std::function<void(const std::string &)> createDebugLogger(bool verbose)
	{
		std::function<void(const std::string &)> debugLogger;
		if (verbose)
			debugLogger = [](const std::string & message) { std::cout << message << std::endl; };

		return debugLogger;
	}

	std::function<void(const std::vector<rts::Duration>&)> createDurationLogger(const std::string & logFile)
	{
		std::shared_ptr<GPIOLogWriter> gpioLogWriter = std::make_shared<GPIOLogWriter>(logFile);
		std::function<void(const std::vector<rts::Duration>&)> durationLogger =
			[gpioLogWriter = std::move(gpioLogWriter)](const std::vector<rts::Duration> & durations) {
				for (const rts::Duration & duration : durations)
					gpioLogWriter->write(duration);
			};

		return durationLogger;
	}

	void play(rts::IFrameTransmitter & transmitter, uint8_t key, rts::SomfyTelisFrame::Action ctrl, uint16_t rollingCode, uint32_t address,
		size_t nRepeatFrames)
	{
		// send 1 normal and 1 repeat frame
		const rts::SomfyTelisFrame frame(key, ctrl, rollingCode, address);
		transmitter.send(frame, nRepeatFrames);
	}

	const std::map<std::string, rts::SomfyTelisFrame::Action> ACTION_NAMES =
	{
		{ "up", rts::SomfyTelisFrame::Action::up },
		{ "down", rts::SomfyTelisFrame::Action::down },
		{ "my", rts::SomfyTelisFrame::Action::my },
		{ "my+down", rts::SomfyTelisFrame::Action::my_down },
		{ "up+down", rts::SomfyTelisFrame::Action::up_down },
		{ "flag", rts::SomfyTelisFrame::Action::flag },
		{ "sun+flag", rts::SomfyTelisFrame::Action::sun_flag },
		{ "prog", rts::SomfyTelisFrame::Action::prog }
	};

	class BackendFactory: public TransmitBackendParametersReceiver
	{
	public:
		virtual void onRPiGPIOBackendParams(unsigned gpioNr) override
		{
			m_createFn = [gpioNr](bool verbose){
				if (verbose)
					std::cout << "Will transmit using RPi GPIO #" << gpioNr << std::endl;

				return rts::FrameTransmitterFactory::create(rts::RPiGPIOTransmitterConfiguration{gpioNr},
						createDebugLogger(verbose)
						// TODO: add negate?
					);
			};
		}

#ifdef RTS_HAVE_GPIOD
		virtual void onGPIODBackendParams(const std::filesystem::path & chipPath, unsigned line, bool openDrain, bool activeLow) override
		{
			m_createFn = [chipPath, line, openDrain, activeLow](bool verbose){
				if (verbose)
					std::cout << "Will transmit using libgpiod chip " << chipPath << ", line #" << line << std::endl;

				return rts::FrameTransmitterFactory::create(
						rts::GPIODTransmitterConfiguration{
							chipPath,
							line,
							openDrain,
							activeLow
						},
						createDebugLogger(verbose)
					);
			};
		}
#endif

		virtual void onESP8266RTSParams(const std::string & host) override
		{
			m_createFn = [host](bool verbose){
				if (verbose)
					std::cout << "Will talk to ESP8266-RTS host " << host << std::endl;

				return rts::FrameTransmitterFactory::create(
						rts::ESP8266RTSTransmitterConfiguration{host},
						createDebugLogger(verbose)
					);
			};
		}

		virtual void onDebugBackendParams(const std::string & logFile) override
		{
			m_createFn = [logFile](bool verbose){
				if (verbose)
					std::cout << "Will log OOK output to " << logFile << std::endl;

				return rts::FrameTransmitterFactory::create(
						rts::DebugTransmitterConfiguration{createDurationLogger(logFile)},
						createDebugLogger(verbose)
					);
			};
		}

		std::shared_ptr<rts::IFrameTransmitter> create(bool verbose)
		{
			return m_createFn(verbose);
		}

	private:
		std::function<std::shared_ptr<rts::IFrameTransmitter>(bool verbose)> m_createFn;
	};
}

// defined in namespace rts because of Argument-dependent lookup
namespace rts
{
	std::istream & operator>>(std::istream & in, SomfyTelisFrame::Action & action)
	{
		std::string token;
		in >> token;

		auto it = ACTION_NAMES.find(token);
		if (it != ACTION_NAMES.end())
			action = it->second;
		else
			in.setstate(std::ios_base::failbit);

		return in;
	}
}

// a simple wrapper around an integral type used solely to let boost::program_options
// parse the input value - to enable hex input (0x...)
template<typename T>
struct Number
{
	static_assert(std::is_integral<T>::value && std::is_unsigned<T>::value, "T must be an unsigned integral type!");
	T value;
};

template<typename T>
std::istream & operator>>(std::istream & in, Number<T> & n)
{
	std::string token;
	in >> token;

	static_assert(std::numeric_limits<T>::max() <= std::numeric_limits<unsigned long long>::max(),
		"T must fit in unsigned long long");

	size_t numConverted = 0;
	n.value = static_cast<T>(std::stoull(token, &numConverted, 0));

	if (numConverted != token.size())
		in.setstate(std::ios_base::failbit);

	return in;
}

int main(int argc, char * argv[])
{
	try
	{
		Number<uint8_t> key;
		rts::SomfyTelisFrame::Action ctrl;
		Number<uint16_t> rollingCode;
		Number<uint32_t> address;
		Number<uint32_t> nRepeatFrames = { DEFAULT_REPEAT_FRAMES };

		std::string allActions;
		for (const auto & actionItem : ACTION_NAMES)
		{
			if (!allActions.empty())
				allActions += ", ";
			allActions += actionItem.first;
		}

		po::options_description toolOptions("Available options");
		toolOptions.add_options()
			("key,k", po::value(&key)->required(),
				"Key value in the packet.")
			("control,c", po::value(&ctrl)->required(),
				(std::string("Control code - what operation shall be done. One of ") + allActions + ".").c_str())
			("rolling-code,r", po::value(&rollingCode)->required(),
				"Rolling code.")
			("address,a", po::value(&address)->required(),
				"Address.")
			("repeat-frames,R", po::value(&nRepeatFrames),
				(std::string("Number of repeat frames. Default: ") + std::to_string(DEFAULT_REPEAT_FRAMES)).c_str())
			("verbose,v",
				"Be verbose, print out what it being transmitted.")
/*			("dry-run,D",
				"Do everything except for the actual transmission.")*/
		;

		BackendFactory backendFactory;
		std::optional<po::variables_map> variablesMap = parseTransmitOptions(argc, argv, toolOptions, backendFactory);
		if (!variablesMap)
			return 0; // help was requested

		const bool verbose = variablesMap->count("verbose") != 0;
		//const bool dryRun = variablesMap->count("dry-run") != 0; TODO: support this

		std::shared_ptr<rts::IFrameTransmitter> transmitter = backendFactory.create(verbose);
		play(*transmitter, key.value, ctrl, rollingCode.value, address.value, nRepeatFrames.value);
	}
	catch (const boost::program_options::error & e)
	{
		std::cerr << e.what() << '\n';
		return 1;
	}

	return 0;
}
