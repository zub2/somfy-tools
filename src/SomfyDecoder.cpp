/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <chrono>
#include <functional>
#include <memory>
#include <thread>

#include <boost/program_options.hpp>

#include "rts/IFrameReceiver.h"
#include "rts/FrameReceiverFactory.h"

#include "SigIntHandler.h"
#include "FrameListener.h"
#include "ParseReceiveOptions.h"

namespace
{
	constexpr double DEFAULT_TOLERANCE = 0.1;

	class BackendFactory: public ReceiveBackendParametersReceiver
	{
	public:
		virtual void onRPiGPIOBackendParams(unsigned gpioNr, size_t bufferSize, const rts::Clock::duration & samplePeriod) override
		{
			m_createFn = [gpioNr, bufferSize, samplePeriod](bool verbose, double tolerance){
				if (verbose)
					std::cout << "Will receive using RPi GPIO #" << gpioNr << std::endl;

				return rts::FrameReceiverFactory::create(
						rts::RPiGPIOReceiverConfiguration{
							gpioNr,
							bufferSize,
							samplePeriod,
							tolerance
						}
					);
			};
		}

#ifdef RTS_HAVE_GPIOD
		virtual void onGPIODBackendParams(const std::filesystem::path & chipPath, unsigned offset, bool activeLow, size_t bufferSize) override
		{
			m_createFn = [chipPath, offset, activeLow, bufferSize](bool verbose, double tolerance){
				if (verbose)
					std::cout << "Will receive using GPIOD chip " << chipPath << ", line " << offset << std::endl;

				return rts::FrameReceiverFactory::create(
						rts::GPIODReceiverConfiguration{
							chipPath,
							offset,
							activeLow,
							bufferSize,
							tolerance
						}
					);
			};
		}
#endif

#ifdef RTS_HAVE_RTLSDR
		virtual void onRTLSDRBackendParams(uint32_t deviceIndex, size_t bufferSize,
				size_t bufferCount, const std::optional<int> & gain, const std::string & logFile) override
		{
			m_createFn = [deviceIndex, bufferSize, bufferCount, gain, logFile](bool verbose, double tolerance){
				if (verbose)
					std::cout << "Will receive using RTL SDR device #" << deviceIndex << std::endl;

				return rts::FrameReceiverFactory::create(
						rts::RTLSDRReceiverConfiguration{
							deviceIndex,
							tolerance,
							bufferSize,
							bufferCount,
							gain,
							logFile
						}
					);
			};
		}
#endif

		std::shared_ptr<rts::IFrameReceiver> create(bool verbose, double tolerance)
		{
			return m_createFn(verbose, tolerance);
		}

	private:
		std::function<std::shared_ptr<rts::IFrameReceiver>(bool verbose, double tolerance)> m_createFn;
	};

	void decode(rts::IFrameReceiver & frameReceiver)
	{
		FrameListener frameListener;

		installSigIntHandler();

		std::thread stopThread([&frameReceiver](){
			waitForSigInt();
			frameReceiver.stop();
		});

		frameReceiver.run(frameListener);
		stopThread.join();
	}
}

int main(int argc, char * argv[])
{
	namespace po = boost::program_options;

	try
	{
		double tolerance = DEFAULT_TOLERANCE;

		boost::program_options::options_description toolOptions("Available options");
		toolOptions.add_options()
			("tolerance,t", boost::program_options::value(&tolerance),
				(std::string("Tolerance in measured timing. Default: ") + std::to_string(tolerance)).c_str())
		;

		BackendFactory backendFactory;
		std::optional<po::variables_map> variablesMap = parseReceiveOptions(argc, argv, toolOptions, backendFactory);
		if (!variablesMap)
			return 0; // help was requested

		const bool verbose = false; //variablesMap->count("verbose") != 0; //TODO: support this

		std::shared_ptr<rts::IFrameReceiver> receiver = backendFactory.create(verbose, tolerance);
		decode(*receiver);
	}
	catch (const boost::program_options::error & e)
	{
		std::cerr << e.what() << '\n';
		return 1;
	}

	return 0;
}
