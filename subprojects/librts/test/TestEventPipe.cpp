/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include "EventPipe.h"

using namespace rts;

BOOST_AUTO_TEST_CASE(TestEventPipe_create)
{
	EventPipe p;
}

BOOST_AUTO_TEST_CASE(TestEventPipe_send, *boost::unit_test::timeout(1))
{
	EventPipe p;

	p.sendEvent();
}

BOOST_AUTO_TEST_CASE(TestEventPipe_sendAndRead, *boost::unit_test::timeout(1))
{
	EventPipe p;

	p.sendEvent();
	p.readEvent();
}
