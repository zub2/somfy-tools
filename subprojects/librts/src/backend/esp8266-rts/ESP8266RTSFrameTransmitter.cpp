/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "backend/esp8266-rts/ESP8266RTSFrameTransmitter.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#include <memory>
#include <cerrno>
#include <system_error>
#include <stdexcept>

#include "GetAddrInfoErr.h"
#include "ESP8266RTSProtocol.h"

namespace
{
	std::unique_ptr<addrinfo, decltype(&freeaddrinfo)> resolveHost(const char * host, const char * port)
	{
		addrinfo hints = {};
		hints.ai_family = AF_UNSPEC; // IPv4 or IPv6
		hints.ai_socktype = SOCK_STREAM; // TCP

		addrinfo * results;
		const int r = getaddrinfo(host, port, &hints, &results);
		if (r != 0)
			throw std::system_error(make_error_code(GAIErrc(r)));

		return {results, freeaddrinfo};
	}

	class Socket
	{
	public:
		Socket(int domain, int type, int protocol):
			m_socket(socket(domain, type, protocol))
		{
			if (m_socket == -1)
			{
				const int e = errno;
				throw std::system_error(e, std::generic_category());
			}
		}

		Socket(const Socket&) = delete;

		Socket(Socket&& other):
			m_socket(other.m_socket)
		{
			other.m_socket = -1;
		}

		Socket & operator=(const Socket&) = delete;
		Socket & operator==(Socket&& other)
		{
			close();
			m_socket = other.m_socket;
			other.m_socket = -1;

			return *this;
		}

		void connect(const sockaddr *addr, socklen_t addrlen)
		{
			if (::connect(m_socket, addr, addrlen) != 0)
			{
				const int e = errno;
				throw std::system_error(e, std::generic_category());
			}
		}

		void write(const std::vector<uint8_t> & data)
		{
			write(data.data(), data.size());
		}

		void write(const uint8_t * data, size_t size)
		{
			ssize_t written = 0;
			while (static_cast<size_t>(written) < size)
			{
				const ssize_t r = ::write(m_socket, data, size);
				if (r > 0)
				{
					written += static_cast<size_t>(r);
					data += r;
				}
				else if (r < 0)
				{
					const int e = errno;
					throw std::system_error(e, std::generic_category());
				}
			}
		}

		void close()
		{
			if (m_socket >= 0)
			{
				::close(m_socket);
				m_socket = -1;
			}
		}

		~Socket()
		{
			close();
		}

	private:
		int m_socket;
	};

	Socket connectTo(const char * host, const char * port)
	{
		auto gaiResults = resolveHost(host, port);

		for (const addrinfo * r = gaiResults.get(); r != nullptr; r = r->ai_next)
		{
			try
			{
				Socket s(r->ai_family, r->ai_socktype, r->ai_protocol);
				s.connect(r->ai_addr, r->ai_addrlen);

				return s;
			}
			catch (const std::system_error &)
			{
				// give up and let the exception propagate if there are no more results
				if (!r->ai_next)
					throw;
				// otherwise try the next result
			}
		}

		throw std::runtime_error("can't look up host: getaddrinfo returned no results");
	}
}

namespace rts
{

ESP8266RTSFrameTransmitter::ESP8266RTSFrameTransmitter(std::string host, std::function<void(const std::string &)> debugLogger):
	m_host(std::move(host)),
	m_debugLogger(std::move(debugLogger))
{
}

void ESP8266RTSFrameTransmitter::send(const SomfyTelisFrame & frame, size_t repeatFrameCount)
{
	Socket s = connectTo(m_host.c_str(), PORT);
	s.write(rts::makeRTSCommand(frame));
}

}
