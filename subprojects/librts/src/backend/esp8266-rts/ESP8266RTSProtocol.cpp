/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ESP8266RTSProtocol.h"

namespace rts
{

const char * PORT = "9772";

namespace
{
	void appendCommandHeader(rts::ByteBuffer & buffer, Command command)
	{
		// append header
		buffer.appendUInt32LE(MAGIC);
		buffer.appendUInt8(VERSION);
		buffer.appendUInt8(static_cast<uint8_t>(command));
	}
}

std::vector<uint8_t> makeRTSCommand(const SomfyTelisFrame & frame)
{
	rts::ByteBuffer buffer;
	appendCommandHeader(buffer, Command::RTSCommand);

	buffer.appendUInt8(frame.getKey());
	buffer.appendUInt8(static_cast<uint8_t>(frame.getCtrl()));
	buffer.appendUInt16LE(frame.getRollingCode());
	buffer.appendUInt32LE(frame.getAddress());

	return buffer.getContent();
}

}
