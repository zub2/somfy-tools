/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <backend/gpiod/GPIODReceiver.h>

#include <sched.h>
#include <poll.h>
#include <string.h>

#include <array>
#include <stdexcept>
#include <system_error>
#include <cerrno>

#include "ThreadPrio.h"

namespace rts
{

GPIODReceiver::GPIODReceiver(const std::filesystem::path& chipPath, unsigned lineOffset, bool activeLow, size_t bufferSize):
	m_lineOffset(lineOffset),
	m_activeLow(activeLow),
	m_chip(chipPath),
	m_transitions(bufferSize)
{
}

void GPIODReceiver::start()
{
	std::lock_guard<std::mutex> g(m_mutex);

	if (m_running)
		throw std::runtime_error("receiving thread already started");

	auto lineSettings = gpiod::line_settings()
		.set_direction(gpiod::line::direction::INPUT)
		.set_edge_detection(gpiod::line::edge::BOTH)
		.set_active_low(m_activeLow);

	m_lineRequest = m_chip.prepare_request()
			.set_consumer("librts")
			.add_line_settings(m_lineOffset, std::move(lineSettings))
			.do_request();

	m_receivingThread = std::thread(&GPIODReceiver::receivingLoop, this);

	setThreadSchedulerAndPrio(m_receivingThread, SCHED_FIFO);

	m_running = true;
	m_runningCondVar.notify_all();
}

void GPIODReceiver::stop()
{
	std::lock_guard<std::mutex> g(m_mutex);

	if (m_running)
	{
		m_running = false;
		m_eventPipe.sendEvent();
		m_dataAvailableCondVar.notify_all(); // if any consumer is waiting, wake them up

		m_receivingThread.join();
	}
}

std::optional<Transition> GPIODReceiver::get()
{
	std::optional<Transition> transition;

	{
		std::unique_lock<std::mutex> g(m_mutex);

		// if still running and there's no data, wait for it
		while (m_transitions.empty() && m_running)
		{
			m_dataAvailableCondVar.wait(g);
		}

		if (!m_transitions.empty())
		{
			transition = m_transitions.front();
			m_transitions.pop_front();
		}
	}

	return transition;
}

void GPIODReceiver::receivingLoop()
{
	// wait for m_running
	{
		std::unique_lock<std::mutex> g(m_mutex);
		while (!m_running)
			m_runningCondVar.wait(g);
	}

	if (!m_lineRequest)
	{
		throw std::logic_error("line request not initialized!");
	}

	std::array<pollfd, 2> pollFDs;
	memset(pollFDs.data(), 0, sizeof(pollFDs[0]) * pollFDs.size());

	pollfd & pollFDLine = pollFDs[0];
	pollFDLine.fd = m_lineRequest->fd();
	pollFDLine.events = POLLIN;

	pollfd & pollFDPipe = pollFDs[1];
	pollFDPipe.fd = m_eventPipe.getReadFd();
	pollFDPipe.events = POLLIN;

	gpiod::edge_event_buffer edgeEventBuffer;

	// Come on libgpiodcxx... a vector allocation just for retrieving the line value!
	// And if I call get_value(0), it's 2 vectors!
	bool currentLevel = m_lineRequest->get_values().front() != gpiod::line::value::INACTIVE;

	while (true)
	{
		const int r = poll(pollFDs.data(), pollFDs.size(), -1);
		if (r < 0)
			throw std::system_error(errno, std::generic_category(), "poll failed");

		if (r > 0)
		{
			if (pollFDPipe.revents != 0)
				break;

			if (pollFDLine.revents != 0)
			{
				if ((pollFDLine.revents & (POLLNVAL | POLLERR)) != 0)
					break;

				const std::size_t numEvents = m_lineRequest->read_edge_events(edgeEventBuffer);
				std::unique_lock<std::mutex> g(m_mutex);
				for (std::size_t i = 0; i < numEvents; i++)
				{
					const auto& event = edgeEventBuffer.get_event(i);
					const bool newLevel = event.type() == gpiod::edge_event::event_type::RISING_EDGE;
					if (currentLevel != newLevel)
					{
						addTransition(event.timestamp_ns(), newLevel);
						currentLevel = newLevel;
					}
				}
			}
		}
	}
}

// the caller must be holding m_mutex when this function is called!
void GPIODReceiver::addTransition(const gpiod::timestamp & timestamp, bool destLevel)
{
	// if we're adding the 1st element, wake up any possible consumer
	if (m_transitions.empty())
		m_dataAvailableCondVar.notify_one();

	// when the buffer is full, the oldest transition is dropped
	m_transitions.push_back(Transition(Clock::time_point(std::chrono::nanoseconds(timestamp.ns())), destLevel));
}

GPIODReceiver::~GPIODReceiver()
{
	stop();
}

}
