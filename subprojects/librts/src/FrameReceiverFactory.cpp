/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FrameReceiverFactory.h"

#include "RPiGPIOFrameReceiver.h"

#ifdef RTS_HAVE_GPIOD
	#include "GPIODFrameReceiver.h"
#endif

#ifdef RTS_HAVE_RTLSDR
	#include "RTLSDRFrameReceiver.h"
#endif

namespace rts
{

std::shared_ptr<IFrameReceiver> FrameReceiverFactory::create(const RPiGPIOReceiverConfiguration & configuration)
{
	return std::make_shared<RPiGPIOFrameReceiver>(configuration.gpioNr, configuration.bufferSize,
			configuration.samplePeriod, configuration.tolerance);
}

#ifdef RTS_HAVE_GPIOD
std::shared_ptr<IFrameReceiver> FrameReceiverFactory::create(const GPIODReceiverConfiguration & configuration)
{
	return std::make_shared<GPIODFrameReceiver>(configuration.chipPath, configuration.offset, configuration.activeLow,
			configuration.bufferSize, configuration.tolerance);
}
#endif

#ifdef RTS_HAVE_RTLSDR
std::shared_ptr<IFrameReceiver> FrameReceiverFactory::create(const RTLSDRReceiverConfiguration & configuration)
{
	return std::make_shared<RTLSDRFrameReceiver>(configuration.deviceIndex, configuration.tolerance,
			configuration.bufferSize, configuration.bufferCount, configuration.gain, configuration.logFile);
}
#endif

}
