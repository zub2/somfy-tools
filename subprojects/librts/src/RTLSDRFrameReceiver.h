/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_RTLSDR_FRAME_RECEIVER_H
#define RTS_RTLSDR_FRAME_RECEIVER_H

#include <memory>
#include <cstdint>
#include <string>
#include <mutex>
#include <optional>

#include "IFrameReceiver.h"
#include "backend/rtlsdr/RTLSDRIQSource.h"

namespace rts
{

class RTLSDRFrameReceiver: public IFrameReceiver
{
public:
	RTLSDRFrameReceiver(uint32_t deviceIndex, double tolerance, size_t bufferSize, size_t bufferCount,
			const std::optional<int> & gain, const std::string & logName);

	virtual void run(IFrameListener & frameListener) override;
	virtual void stop() override;

private:
	std::mutex m_stopMutex;
	bool m_stopped = false;

	uint32_t m_deviceIndex;
	double m_tolerance;
	size_t m_bufferSize;
	size_t m_bufferCount;
	std::optional<int> m_gain;
	std::string m_logName;

	std::unique_ptr<RTLSDRIQSource> m_rtlSDRIQSource;
};

}

#endif // RTS_RTLSDR_FRAME_RECEIVER_H
