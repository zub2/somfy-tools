/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "GPIODFrameReceiver.h"

#include "DurationTracker.h"
#include "SomfyDecoder.h"

namespace rts
{

GPIODFrameReceiver::GPIODFrameReceiver(const std::string & gpioChip, unsigned offset, bool activeLow, size_t bufferSize, double tolerance):
	m_gpioChip(gpioChip),
	m_offset(offset),
	m_activeLow(activeLow),
	m_bufferSize(bufferSize),
	m_tolerance(tolerance)
{}

void GPIODFrameReceiver::run(IFrameListener & frameListener)
{
	std::unique_lock lock(m_stopMutex);

	// if stop was called before this is reached, then just don't start
	if (m_stopped)
		return;

	m_gpiodReceiver = std::make_unique<GPIODReceiver>(m_gpioChip, m_offset, m_activeLow, m_bufferSize);

	DurationTracker<GPIODReceiver> durationTracker(*m_gpiodReceiver);
	SomfyDecoder<DurationTracker<GPIODReceiver>> decoder(durationTracker, m_tolerance, frameListener);

	m_gpiodReceiver->start();

	// let stop() proceed
	lock.unlock();

	decoder.run();
}

void GPIODFrameReceiver::stop()
{
	std::scoped_lock lock(m_stopMutex);
	if (!m_stopped)
	{
		m_stopped = true;
		if (m_gpiodReceiver)
			m_gpiodReceiver->stop();
	}
}

}
