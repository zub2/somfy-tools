/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RTLSDRFrameReceiver.h"

#include "backend/rtlsdr/RTLSDRDevice.h"
#include "backend/rtlsdr/OOKDecoder.h"
#include "DurationTracker.h"
#include "SomfyDecoder.h"

namespace rts
{

RTLSDRFrameReceiver::RTLSDRFrameReceiver(uint32_t deviceIndex, double tolerance, size_t bufferSize, size_t bufferCount,
		const std::optional<int> & gain, const std::string & logName):
	m_deviceIndex(deviceIndex),
	m_tolerance(tolerance),
	m_bufferSize(bufferSize),
	m_bufferCount(bufferCount),
	m_gain(gain),
	m_logName(logName)
{}

void RTLSDRFrameReceiver::run(IFrameListener & frameListener)
{
	std::unique_lock lock(m_stopMutex);

	// if stop was called before this is reached, then just don't start
	if (m_stopped)
		return;

	RTLSDRDevice rtlSDRDevice(m_deviceIndex);

	rtlSDRDevice.setFrequency(RTLSDRDevice::RTS_FREQUENCY);
	rtlSDRDevice.setSampleRate(RTLSDRDevice::DEFUALT_SAMPLE_RATE);
	rtlSDRDevice.setManualTunnerGainMode(m_gain != std::nullopt);
	if (m_gain)
		rtlSDRDevice.setTunerGain(*m_gain);

	m_rtlSDRIQSource = std::make_unique<RTLSDRIQSource>(rtlSDRDevice, m_bufferSize, m_bufferCount, m_logName);
	rts::OOKDecoder<rts::RTLSDRIQSource> ookDecoder(*m_rtlSDRIQSource, RTLSDRDevice::DEFUALT_SAMPLE_RATE);
	rts::DurationTracker<rts::OOKDecoder<rts::RTLSDRIQSource>> durationTracker(ookDecoder);
	rts::SomfyDecoder decoder(durationTracker, m_tolerance, frameListener);

	m_rtlSDRIQSource->start();

	// let stop() proceed
	lock.unlock();

	decoder.run();
}

void RTLSDRFrameReceiver::stop()
{
	std::scoped_lock lock(m_stopMutex);
	if (!m_stopped)
	{
		m_stopped = true;
		if (m_rtlSDRIQSource)
			m_rtlSDRIQSource->stop();
	}
}

}
