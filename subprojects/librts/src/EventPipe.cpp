/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "EventPipe.h"

#include <system_error>
#include <cerrno>

#include <unistd.h>

namespace rts
{

EventPipe::EventPipe()
{
	int pipeFds[2];

	if (pipe(pipeFds) != 0)
		throw std::system_error(errno, std::generic_category(), "can't create pipe");

	m_readFd = pipeFds[0];
	m_writeFd = pipeFds[1];
}

void EventPipe::sendEvent()
{
	char c = 0;
	int r = 0;

	while (r == 0)
		r = write(m_writeFd, &c, 1);

	if (r == -1)
		throw std::system_error(errno, std::generic_category(), "can't write into pipe");
}

void EventPipe::readEvent()
{
	char c;
	int r = 0;

	while (r == 0)
		r = read(m_readFd, &c, 1);

	if (r == -1)
		throw std::system_error(errno, std::generic_category(), "can't read from pipe");
}

int EventPipe::getReadFd() const
{
	return m_readFd;
}

EventPipe::~EventPipe()
{
	close(m_readFd);
	close(m_writeFd);
}

} // namespace rts
