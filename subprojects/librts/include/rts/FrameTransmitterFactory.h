/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_FRAME_TRANSMITTER_FACTORY_H
#define RTS_FRAME_TRANSMITTER_FACTORY_H

#include <string>
#include <filesystem>
#include <memory>
#include <functional>

#include "Duration.h"
#include "IFrameTransmitter.h"

namespace rts
{

struct DummyTransmitterConfiguration
{};

struct RPiGPIOTransmitterConfiguration
{
	unsigned gpioNr;
};

#ifdef RTS_HAVE_GPIOD
struct GPIODTransmitterConfiguration
{
	std::filesystem::path chipPath;
	unsigned offset;
	bool openDrain;
	bool activeLow;
};
#endif

struct ESP8266RTSTransmitterConfiguration
{
	std::string hostname;
};

struct DebugTransmitterConfiguration
{
	std::function<void(const std::vector<Duration>&)> durationLogger;
};

struct FrameTransmitterFactory
{
	static std::shared_ptr<IFrameTransmitter> create(const DummyTransmitterConfiguration & configuration,
			std::function<void(const std::string &)> debugLogger);

	static std::shared_ptr<IFrameTransmitter> create(const RPiGPIOTransmitterConfiguration & configuration,
			std::function<void(const std::string &)> debugLogger);

#ifdef RTS_HAVE_GPIOD
	static std::shared_ptr<IFrameTransmitter> create(const GPIODTransmitterConfiguration & configuration,
			std::function<void(const std::string &)> debugLogger);
#endif

	static std::shared_ptr<IFrameTransmitter> create(const ESP8266RTSTransmitterConfiguration & configuration,
			std::function<void(const std::string &)> debugLogger);

	static std::shared_ptr<IFrameTransmitter> create(const DebugTransmitterConfiguration & configuration,
			std::function<void(const std::string &)> debugLogger);
};

}

#endif // RTS_FRAME_TRANSMITTER_FACTORY_H
