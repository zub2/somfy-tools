/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_IFRAME_RECEIVER_H
#define RTS_IFRAME_RECEIVER_H

#include <vector>

#include "SomfyFrameType.h"
#include "SomfyFrame.h"
#include "IFrameListener.h"

namespace rts
{

class IFrameReceiver
{
public:
	/**
	 * Run the receiver.
	 *
	 * This can be called at most once on an instance of frame receiver. The function
	 * blocks until stop() is called or an error occurs (in which case an exception
	 * is thrown).
	 */
	virtual void run(IFrameListener & frameListener) = 0;

	/**
	 * Stop the receiver.
	 *
	 * This can be called at any time. If it's called before start(), the subsequent
	 * call to start() returns immediately.
	 */
	virtual void stop() = 0;

	virtual ~IFrameReceiver() = default;
};

}

#endif // RTS_IFRAME_RECEIVER_H
