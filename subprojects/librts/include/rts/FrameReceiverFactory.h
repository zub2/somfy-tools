/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_FRAME_RECEIVER_FACTORY_H
#define RTS_FRAME_RECEIVER_FACTORY_H

#include <string>
#include <filesystem>
#include <memory>
#include <optional>

#include "Clock.h"
#include "IFrameReceiver.h"

namespace rts
{

struct RPiGPIOReceiverConfiguration
{
	unsigned gpioNr;
	size_t bufferSize;
	Clock::duration samplePeriod;
	double tolerance;
};

#ifdef RTS_HAVE_GPIOD
struct GPIODReceiverConfiguration
{
	const std::filesystem::path chipPath;
	unsigned offset;
	bool activeLow;
	size_t bufferSize;
	double tolerance;
};
#endif

#ifdef RTS_HAVE_RTLSDR
struct RTLSDRReceiverConfiguration
{
	uint32_t deviceIndex;
	double tolerance;
	size_t bufferSize;
	size_t bufferCount;
	std::optional<int> gain;
	std::string logFile;
};
#endif

struct FrameReceiverFactory
{
	static std::shared_ptr<IFrameReceiver> create(const RPiGPIOReceiverConfiguration & configuration);

#ifdef RTS_HAVE_GPIOD
	static std::shared_ptr<IFrameReceiver> create(const GPIODReceiverConfiguration & configuration);
#endif

#ifdef RTS_HAVE_RTLSDR
	static std::shared_ptr<IFrameReceiver> create(const RTLSDRReceiverConfiguration & configuration);
#endif
};

}

#endif // RTS_FRAME_RECEIVER_FACTORY_H
