/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_IFRAME_LISTENER_H
#define RTS_IFRAME_LISTENER_H

#include <vector>

#include "SomfyFrameType.h"
#include "SomfyFrame.h"

namespace rts
{

/**
 * @brief A high-level interface for receiving RTS frames via a backend.
 */
class IFrameListener
{
public:
	/**
	 * The header of a RTS frame was detected.
	 *
	 * This is called for each detected frame header. After this either
	 * onFrameDecoded is called (if the decoding is successful) or
	 * onFrameDecodeError is called (if decoding fails). Only then can
	 * onFrameDetected be called again.
	 */
	virtual void onFrameDetected(SomfyRemoteType remoteType, SomfyFrameType frameType) = 0;

	/**
	 * A Telis frame has been successfully decoded.
	 */
	virtual void onFrameDecoded(SomfyFrameType frameType, const SomfyTelisFrame & frame) = 0;

	/**
	 * A Keytis fram has been successfully decoded.
	 */
	virtual void onFrameDecoded(SomfyFrameType frameType, const SomfyKeytisFrame & frame) = 0;

	enum class FrameError
	{
		DecodingError,
		WrongChecksum
	};

	/**
	 * Decoding of a RTS frame failed.
	 *
	 * @param[in] bits The bits that were decoded. The payload is incomplete
	 * and possibly corrupted.
	 */
	virtual void onFrameDecodeError(FrameError error, const std::vector<bool> & bits) = 0;

protected:
	virtual ~IFrameListener() = default;
};

}

#endif // RTS_IFRAME_LISTENER_H
