/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_SOMFY_FRAME_MATCHER
#define RTS_SOMFY_FRAME_MATCHER

#include <optional>
#include <cstddef>
#include <vector>

#include "Clock.h"
#include "Duration.h"
#include "SomfyFrameType.h"

namespace rts
{

struct SomfyFrameHeader;

class SomfyFrameMatcher
{
public:
	struct FrameMatch
	{
		FrameMatch(SomfyRemoteType remoteType, SomfyFrameType frameType, const Clock::duration & remainingDuration, bool state):
			remoteType(remoteType),
			frameType(frameType),
			remainingDuration(remainingDuration),
			state(state)
		{}

		FrameMatch():
			remoteType(SomfyRemoteType::telis),
			frameType(SomfyFrameType::normal),
			remainingDuration(Clock::duration::zero()),
			state(false)
		{}

		SomfyRemoteType remoteType;
		SomfyFrameType frameType;
		Clock::duration remainingDuration;
		bool state;
	};

	SomfyFrameMatcher(double tolerance);

	std::optional<FrameMatch> newTransition(const Duration & duration);
	void reset();

private:
	class SequenceMatcher
	{
	public:
		SequenceMatcher(const SomfyFrameHeader& frameHeader);
		std::optional<Clock::duration> newTransition(const Duration & duration, double tolerance);
		void reset();
	private:
		bool matchDuration(Clock::duration actual, Clock::duration expected, double tolerance);
		bool matchDurationAtLeast(Clock::duration actual, Clock::duration expected, double tolerance);
		const Duration *m_sequence;
		const size_t m_sequenceSize;
		size_t m_matchedCount;
	};

	const double m_tolerance;

	struct MatcherEntry
	{
		SequenceMatcher matcher;
		SomfyRemoteType remoteType;
		SomfyFrameType frameType;
	};

	std::vector<MatcherEntry> m_matchers;
};

} // namespace rts

#endif // RTS_SOMFY_FRAME_MATCHER
