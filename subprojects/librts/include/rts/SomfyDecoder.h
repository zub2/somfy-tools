/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_SOMFY_DECODER_H
#define RTS_SOMFY_DECODER_H

#include <iostream>
#include <iomanip>
#include <sstream>
#include <optional>
#include <stdexcept>

#include "Duration.h"
#include "ManchesterDecoder.h"
#include "SomfyFrameMatcher.h"
#include "SomfyFrameType.h"
#include "SomfyFrame.h"
#include "IFrameListener.h"

namespace rts
{

template<typename Source>
class SomfyDecoder
{
private:
	enum class State
	{
		SearchingForFrame,
		ReadingPayload
	};

public:
	SomfyDecoder(Source & s, double tolerance, IFrameListener & listener):
		m_source(s),
		m_tolerance(tolerance),
		m_listener(listener)
	{}

	void run()
	{
		SomfyFrameMatcher matcher(m_tolerance);
		ManchesterDecoder decoder(std::max(SomfyTelisFrame::FRAME_SIZE, SomfyKeytisFrame::FRAME_SIZE) * CHAR_BIT);

		State state = State::SearchingForFrame;
		SomfyFrameMatcher::FrameMatch frameMatch;
		std::size_t expectedBits = 0;

		std::optional<Duration> duration = m_source.get();
		while (duration)
		{
			switch (state)
			{
			case State::SearchingForFrame:
				if (auto f = matcher.newTransition(*duration))
				{
					frameMatch = *f;
					expectedBits = getExpectedFrameSizeInBits(frameMatch.remoteType);
					m_listener.onFrameDetected(frameMatch.remoteType, frameMatch.frameType);
					state = State::ReadingPayload;
					decoder.reset();

					// pass reminder of pulse to the next state (ReadingPayload)
					if (frameMatch.remainingDuration > Clock::duration::zero())
						duration = Duration(frameMatch.remainingDuration, frameMatch.state);
					else
						duration.reset();
				}
				else
					duration.reset();
				break;

			case State::ReadingPayload:
				if (decoder.newTransition(*duration))
				{
					if (decoder.getBits().size() == expectedBits)
					{
						onFrameReceived(frameMatch.remoteType, frameMatch.frameType, decoder.getBits());
						state = State::SearchingForFrame;
					}
					// else: go on
				}
				else
				{
					m_listener.onFrameDecodeError(IFrameListener::FrameError::DecodingError, decoder.getBits());
					state = State::SearchingForFrame;
				}
				duration.reset();
				break;
			}

			if (!duration)
				duration = m_source.get();
		}
	}

private:
	void onFrameReceived(SomfyRemoteType remoteType, SomfyFrameType frameType, const std::vector<bool> & bits)
	{
		switch (remoteType)
		{
		case SomfyRemoteType::telis:
			onFrameReceivedImpl<SomfyTelisFrame>(remoteType, frameType, bits);
			return;
		case SomfyRemoteType::keytis:
			onFrameReceivedImpl<SomfyKeytisFrame>(remoteType, frameType, bits);
			return;
		}
		throw std::runtime_error("Unexpected remote type");
	}

	template<typename REMOTE_TYPE>
	void onFrameReceivedImpl(SomfyRemoteType remoteType, SomfyFrameType frameType, const std::vector<bool> & bits)
	{
		std::vector<uint8_t> bytes = bitsToBytes(bits);

		try
		{
			REMOTE_TYPE frame = REMOTE_TYPE::fromBytes(std::move(bytes));
			m_listener.onFrameDecoded(frameType, frame);
		}
		catch (const WrongFrameChecksumException &)
		{
			m_listener.onFrameDecodeError(IFrameListener::FrameError::WrongChecksum, bits);
		}
	}

	std::vector<uint8_t> bitsToBytes(const std::vector<bool> & bits)
	{
		if (bits.size() % CHAR_BIT != 0)
			throw std::runtime_error("Bit count not a multiple of byte size!");

		std::vector<uint8_t> bytes;
		bytes.reserve(bits.size() / CHAR_BIT);
		for (size_t i = 0; i < bits.size(); i++)
		{
			if (i % CHAR_BIT == 0)
				bytes.push_back(0);

			if (bits[i])
			{
				const uint8_t bitWeight = 1 << (CHAR_BIT - 1 - (i % CHAR_BIT));
				bytes.back() |= bitWeight;
			}
		}

		return bytes;
	}

	static std::size_t getExpectedFrameSizeInBits(SomfyRemoteType remoteType)
	{
		switch (remoteType)
		{
		case SomfyRemoteType::telis:
			return SomfyTelisFrame::FRAME_SIZE * CHAR_BIT;
		case SomfyRemoteType::keytis:
			return SomfyKeytisFrame::FRAME_SIZE * CHAR_BIT;
		}

		throw std::runtime_error("Unexpected remote type");
	}

	Source & m_source;
	const double m_tolerance;
	IFrameListener & m_listener;
};

} // namespace rts

#endif // RTS_SOMFY_DECODER_H
