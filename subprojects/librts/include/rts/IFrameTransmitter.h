/*
 * Copyright 2018, 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_IFRAME_TRANSMITTER_H
#define RTS_IFRAME_TRANSMITTER_H

#include <cstddef>
#include "SomfyFrame.h"

namespace rts
{

/**
 * @brief A high-level interface for sending RTS frames via a backend.
 */
class IFrameTransmitter
{
public:
	/**
	 * Send a Telis frame.
	 *
	 * Always sends 1 normal frame followed by a selected number of repeat frames.
	 *
	 * @param[in] frame The frame to send.
	 * @param[in] repeatFrameCount Number of repeat frames that should follow the normal frame.
	 */
	virtual void send(const SomfyTelisFrame & frame, size_t repeatFrameCount) = 0;

	// TODO: add support for sending a Keytis frame

	virtual ~IFrameTransmitter()
	{}
};

}

#endif // RTS_IFRAME_TRANSMITTER_H
