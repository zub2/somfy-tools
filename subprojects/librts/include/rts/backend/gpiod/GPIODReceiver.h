/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_GPIOD_RECEIVER_H
#define RTS_GPIOD_RECEIVER_H

#include <string>
#include <optional>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <filesystem>

#include <boost/circular_buffer.hpp>

#include <gpiod.hpp>

#include "../../Clock.h"
#include "../../Transition.h"
#include "../../EventPipe.h"

namespace rts
{

class GPIODReceiver
{
public:
	GPIODReceiver(const std::filesystem::path& chipPath, unsigned lineOffset, bool activeLow, size_t bufferSize);

	void start();
	void stop();

	std::optional<Transition> get();

	~GPIODReceiver();

private:
	void receivingLoop();
	void addTransition(const gpiod::timestamp & timestamp, bool destLevel);

	const gpiod::line::offset m_lineOffset;
	const bool m_activeLow;

	std::mutex m_mutex;
	std::thread m_receivingThread;

	gpiod::chip m_chip;
	std::optional<gpiod::line_request> m_lineRequest;
	bool m_running = false;
	std::condition_variable m_runningCondVar;
	std::condition_variable m_dataAvailableCondVar;

	EventPipe m_eventPipe;

	boost::circular_buffer<Transition> m_transitions;
};

}

#endif // RTS_GPIOD_RECEIVER_H
