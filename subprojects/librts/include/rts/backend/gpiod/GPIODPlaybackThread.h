/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_GPIOD_GPIOD_PLAYBACK_THREAD_H
#define RTS_GPIOD_GPIOD_PLAYBACK_THREAD_H

#include <gpiod.hpp>

#include "../PlaybackThreadBase.h"

namespace rts
{

class GPIODPlaybackThread: public PlaybackThreadBase<GPIODPlaybackThread>
{
public:
	GPIODPlaybackThread(gpiod::line_request & lineRequest);

private:
	friend class PlaybackThreadBase<GPIODPlaybackThread>;

	void write(bool value);

	gpiod::line_request & m_lineRequest;
	const gpiod::line::offsets m_offsets;
	gpiod::line::values m_values;
};

} // namespace rts

#endif // RTS_GPIOD_GPIOD_PLAYBACK_THREAD_H
