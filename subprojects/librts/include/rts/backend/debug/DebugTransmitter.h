/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rts-tools.
 *
 * rts-tools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rts-tools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rts-tools.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RTS_DEBUG_TRANSMITTER_H
#define RTS_DEBUG_TRANSMITTER_H

#include <functional>

#include "Duration.h"

namespace rts
{

class DebugTransmitter
{
public:
	DebugTransmitter(std::function<void(const std::vector<Duration>&)> durationLogger):
		m_durationLogger(std::move(durationLogger))
	{}

	void transmit(const std::vector<Duration> & samples);

private:
	std::function<void(const std::vector<Duration>&)> m_durationLogger;
};

}

#endif // RTS_DEBUG_TRANSMITTER_H
